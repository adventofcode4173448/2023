package com.bernonconsulting;

import java.math.BigInteger;

import static java.math.BigInteger.ONE;

public class SourceDestinationMapper {

    private BigInteger destinationStart;
    private BigInteger sourceStart;
    private BigInteger range;

    public SourceDestinationMapper(String destinationStart, String sourceStart, String range) {
        this.destinationStart = new BigInteger(destinationStart);
        this.sourceStart = new BigInteger(sourceStart);
        this.range = new BigInteger(range).subtract(ONE);
    }

    public boolean isInRange(BigInteger source) {
//        return source >= sourceStart && source <= sourceStart + range;
        return source.compareTo(sourceStart) >= 0 && source.compareTo(sourceStart.add(range)) <= 0;
    }

    public BigInteger mapSourceToDestination(BigInteger source) {
        if (isInRange(source)) {
            return destinationStart.add(source).subtract(sourceStart);
        }
        throw new RuntimeException(String.format("source %d not in range %d - %d", source, sourceStart, sourceStart.add(range)));
    }
}