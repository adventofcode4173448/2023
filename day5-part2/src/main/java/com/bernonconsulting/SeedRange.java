package com.bernonconsulting;

import java.math.BigInteger;

public record SeedRange(BigInteger start, BigInteger range) {
}
