package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

//https://adventofcode.com/2023/day/5#part2
//Whooops forgot to save part 1.
public class Main {
    private static final Pattern numberPattern = Pattern.compile("\\d+");

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            Instant start = Instant.now();
            List<String> lines = bufferedReader.lines().toList();
            Matcher seedMatcher = numberPattern.matcher(lines.get(0));
            List<BigInteger> numbers = seedMatcher.results()
                    .map(MatchResult::group)
                    .map(BigInteger::new)
                    .toList();
            List<SeedRange> seedsRanges = calculateSeedRanges(numbers);

            List<List<SourceDestinationMapper>> sourceDestinationMaps = readAlmanac(lines);

            BigInteger result = seedsRanges.stream()
                    .map(seedRange -> {
                        BigInteger end = seedRange.start().add(seedRange.range());
                        return Stream.iterate(seedRange.start(), i -> i.compareTo(end) < 0, i -> i.add(BigInteger.ONE))
                                .map(currentSeed -> {
                                    BigInteger currentValue = currentSeed;
                                    for (List<SourceDestinationMapper> maps : sourceDestinationMaps) {
                                        for (SourceDestinationMapper mapper : maps) {
                                            if (mapper.isInRange(currentValue)) {
                                                currentValue = mapper.mapSourceToDestination(currentValue);
                                                break;
                                            }
                                        }
                                    }
                                    return currentValue;
                                })
                                .reduce((a, b) -> a.compareTo(b) < 0 ? a : b)
                                .orElseThrow();
                        })
                    .reduce((a, b) -> a.compareTo(b) < 0 ? a : b)
                    .orElseThrow();

            System.out.println("Duration:" + Duration.between(start, Instant.now()));
            System.out.println(result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }

    }

    private static List<SeedRange> calculateSeedRanges(List<BigInteger> numbers) {
        List<SeedRange> seeds = new ArrayList<>();

        for (int i = 0; i < numbers.size(); i = i + 2) {
            seeds.add(new SeedRange(numbers.get(i), numbers.get(i + 1)));
        }
        return seeds;
    }

    private static List<List<SourceDestinationMapper>> readAlmanac(List<String> source) {
        List<List<SourceDestinationMapper>> result = new ArrayList<>();
        boolean readingMap = false;
        List<SourceDestinationMapper> mappers = new ArrayList<>();
        for (String line : source) {
            if (line.isEmpty() && readingMap) {
                readingMap = false;
            }
            if (readingMap) {
                Matcher numberMatcher = numberPattern.matcher(line);
                List<String> matches = numberMatcher.results()
                        .map(MatchResult::group)
                        .toList();
                mappers.add(new SourceDestinationMapper(matches.get(0), matches.get(1), matches.get(2)));
            }
            if (line.contains("map:")) {
                readingMap = true;
                mappers = new ArrayList<>();
                result.add(mappers);
                System.out.println("reading " + line);
            }
        }
        return result;
    }
}