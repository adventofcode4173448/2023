package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/4
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {

            List<String> cards = bufferedReader.lines().toList();
            int result = cards.stream()
                    .mapToInt(Main::calculatePoints)
                    .sum();
            System.out.println("result: " + result);

        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static Integer calculatePoints(String card) {
        Integer cardPoints = 0;
        Pattern numberRegex = Pattern.compile("\\d+");
        String winningNumbers = card.substring(card.indexOf(":"), card.indexOf("|"));
        String myNumbers = card.substring(card.indexOf("|"));
        Matcher winningMatcher = numberRegex.matcher(winningNumbers);

        List<String> winningNumbersList = new ArrayList<>();
        while (winningMatcher.find()) {
            winningNumbersList.add(winningMatcher.group());
        }

        Matcher myMatcher = numberRegex.matcher(myNumbers);
        while (myMatcher.find()) {
            if (winningNumbersList.contains(myMatcher.group())) {
                cardPoints = cardPoints == 0
                        ? 1
                        : cardPoints*2;
            }
        }
        return cardPoints;
    }


}