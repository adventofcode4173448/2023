package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/7
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            List<Hand> hands = bufferedReader.lines()
                    .map(line -> new Hand(line.substring(0, 5), Integer.parseInt(line.substring(6))))
                    .sorted()
                    .toList();

            int result = calculateWinnings(hands);
            System.out.println("result: " + result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static int calculateWinnings(List<Hand> hands) {
        AtomicInteger rank = new AtomicInteger(1);
        return hands.stream()
                .mapToInt(hand -> hand.getBid() * rank.getAndIncrement())
                .sum();
    }
}