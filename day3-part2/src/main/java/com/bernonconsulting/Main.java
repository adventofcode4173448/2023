package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/3#part2
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {

            List<String> schematic = bufferedReader.lines().toList();
            int result = findPartNumbers(schematic)
                    .stream()
                    .mapToInt(a -> a)
                    .sum();
            System.out.println("result: " + result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static List<Integer> findPartNumbers(List<String> schematic) {
        Pattern gearRegex = Pattern.compile("[*]");
        List<Integer> gears = new ArrayList<>();

        for (int rowNumber = 0; rowNumber < schematic.size(); rowNumber++) {
            String row = schematic.get(rowNumber);
            Matcher gearMatcher = gearRegex.matcher(row);
            while (gearMatcher.find()) {
                int gearIndex = gearMatcher.start();

                List<String> surroundingRows = schematic.subList(
                        Math.max(rowNumber - 1, 0),
                        Math.min(schematic.size(), rowNumber + 2)
                );

                if (isPartOfGear(gearIndex, surroundingRows)) {
                    gears.add(calculateGear(gearIndex, surroundingRows));
                }
            }
        }
        return gears;
    }

    private static Integer calculateGear(int index, List<String> surroundingRows) {
        Pattern numberRegex = Pattern.compile("\\d+");

        List<Integer> gearNumbers = new ArrayList<>();
        surroundingRows.forEach(row -> {
            Matcher numberMatcher = numberRegex.matcher(row);
            while (numberMatcher.find()) {
                if (numberMatcher.start() <= index + 1 && numberMatcher.end() >= index) {
                    gearNumbers.add(Integer.valueOf(numberMatcher.group()));
                }
            }
        });
        if (gearNumbers.size() != 2) {
            throw new RuntimeException("unexpected amount of numbers in gear");
        }
        return gearNumbers.get(0) * gearNumbers.get(1);
    }

    private static boolean isPartOfGear(int index, List<String> rows) {
        Pattern numberRegex = Pattern.compile("\\d+");

        int foundNumbers = 0;
        for (String row : rows) {
            String subString = row.substring(index - 1, index + 2);
            Matcher matcher = numberRegex.matcher(subString);
            while (matcher.find()) {
                foundNumbers++;
            }
        }

        return foundNumbers > 1;
    }
}