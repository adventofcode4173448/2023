package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/4#part2
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {

            List<String> cards = bufferedReader.lines().toList();
            Map<Integer, Integer> cardsMap = new HashMap<>();
            Pattern numberRegex = Pattern.compile("\\d+");
            cards.forEach(card -> {
                Matcher numberMatcher = numberRegex.matcher(card);
                numberMatcher.find();
                Integer cardNumber = Integer.valueOf(numberMatcher.group());
                cardsMap.put(cardNumber, 1);
            });

            for (int i = 0; i < cards.size(); i++) {
                int numberOfMatches = calculateNumberOfMatches(cards.get(i));
                int currentCard = i +1;
                int amountOfCopiesOfThisCard = cardsMap.get(currentCard);
                int cardToCopy = currentCard+1;
                for (int j = 1; j <= numberOfMatches; j++) {
                    cardsMap.compute(cardToCopy++, (k, v) -> v + 1*amountOfCopiesOfThisCard);
                }
            }

            int result = cardsMap.values().stream().mapToInt(a ->a).sum();
            System.out.println("result: " + result);

        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static Integer calculateNumberOfMatches(String card) {
        Integer numberOfMatches = 0;
        Pattern numberRegex = Pattern.compile("\\d+");
        String winningNumbers = card.substring(card.indexOf(":"), card.indexOf("|"));
        String myNumbers = card.substring(card.indexOf("|"));
        Matcher winningMatcher = numberRegex.matcher(winningNumbers);

        List<String> winningNumbersList = new ArrayList<>();
        while (winningMatcher.find()) {
            winningNumbersList.add(winningMatcher.group());
        }

        Matcher myMatcher = numberRegex.matcher(myNumbers);
        while (myMatcher.find()) {
            if (winningNumbersList.contains(myMatcher.group())) {
                numberOfMatches++;
            }
        }
        return numberOfMatches;
    }


}