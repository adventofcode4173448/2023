package com.bernonconsulting;

import org.apache.commons.lang3.StringUtils;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map;

public class Hand implements Comparable<Hand> {

    private static final Map<String, Integer> cardMap = Map.ofEntries(
            new SimpleEntry<>("A", 14),
            new SimpleEntry<>("K", 13),
            new SimpleEntry<>("Q", 12),
            new SimpleEntry<>("T", 11),
            new SimpleEntry<>("9", 10),
            new SimpleEntry<>("8", 9),
            new SimpleEntry<>("7", 8),
            new SimpleEntry<>("6", 7),
            new SimpleEntry<>("5", 6),
            new SimpleEntry<>("4", 5),
            new SimpleEntry<>("3", 4),
            new SimpleEntry<>("2", 3),
            new SimpleEntry<>("1", 2),
            new SimpleEntry<>("J", 1)
    );

    private final String cards;
    private final int bid;
    private final Type type;

    public Hand(String cards, int bid) {
        this.cards = cards;
        this.bid = bid;
        this.type = Type.fromCards(cards);
    }

    public String getCards() {
        return cards;
    }

    public int getBid() {
        return bid;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Hand{" +
                "cards='" + cards + '\'' +
                ", bid=" + bid +
                ", type=" + type +
                '}';
    }

    @Override
    public int compareTo(Hand o) {
        if (type != o.getType()) {
            return type.compareTo(o.getType());
        }
        for (int i = 0; i < cards.length(); i++) {
            String thisCard = cards.substring(i, i + 1);
            String otherCard = o.getCards().substring(i, i + 1);
            if (!thisCard.equals(otherCard)) {
                return cardMap.get(thisCard).compareTo(cardMap.get(otherCard));
            }
        }
        System.out.println("Duplicate hand");
        return 0;
    }
}

enum Type {
    HIGH_CARD,
    ONE_PAIR,
    TWO_PAIR,
    THREE_OF_A_KIND,
    FULL_HOUSE,
    FOUR_OF_A_KIND,
    FIVE_OF_A_KIND;

    public static Type fromCards(String cards) {
        if (isFiveOfAKind(cards)) {
            return FIVE_OF_A_KIND;
        }
        if (isFourOfAKind(cards)) {
            return FOUR_OF_A_KIND;
        }

        int numberOfPairs = findNumberOfPairs(cards);
        int numberOfJokers = StringUtils.countMatches(cards, "J");

        if (numberOfJokers == 0) {
            if (hasTriplet(cards)) {
                if (numberOfPairs == 1) {
                    return FULL_HOUSE;
                } else {
                    return THREE_OF_A_KIND;
                }
            }

            if (numberOfPairs == 2) {
                return TWO_PAIR;
            }
            if (numberOfPairs == 1) {
                return ONE_PAIR;
            }
            return HIGH_CARD;
        }
        if (numberOfJokers == 1) {
            if (numberOfPairs == 2) {
                return FULL_HOUSE;
            }
            if (numberOfPairs == 1) {
                return THREE_OF_A_KIND;
            } else
                return ONE_PAIR;
        }
        return THREE_OF_A_KIND;
    }

    private static boolean isFiveOfAKind(String cards) {
        int numberOfJokers = StringUtils.countMatches(cards, "J");
        cards = cards.replace("J", "");
        if (numberOfJokers == 5) {
            return true;
        }
        return StringUtils.countMatches(cards, cards.substring(0, 1)) + numberOfJokers == 5;
    }

    private static boolean isFourOfAKind(String cards) {
        int numberOfJokers = StringUtils.countMatches(cards, "J");
        cards = cards.replace("J", "");
        return StringUtils.countMatches(cards, cards.substring(0, 1)) + numberOfJokers == 4
                || StringUtils.countMatches(cards, cards.substring(1, 2)) + numberOfJokers == 4;
    }

    private static boolean hasTriplet(String cards) {
        cards = cards.replace("J", "");
        for (int i = 0; i < cards.length(); i++) {
            String currentCard = cards.substring(i, i + 1);
            if (StringUtils.countMatches(cards, currentCard) == 3) {
                return true;
            }
        }
        return false;
    }

    private static int findNumberOfPairs(String cards) {
        cards = cards.replace("J", "");
        int numberOfPairs = 0;
        for (int i = 0; i < cards.length(); i++) {
            String currentCard = cards.substring(i, i + 1);
            if (StringUtils.countMatches(cards, currentCard) == 2) {
                numberOfPairs++;
                cards = cards.replace(currentCard, "");
            }
        }
        return numberOfPairs;
    }
}
