package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//https://adventofcode.com/2023/day/8
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            List<String> lines = bufferedReader.lines().toList();
            Instructions instructions = new Instructions(lines.get(0).toCharArray());

            Map<String, Node> nodes = new HashMap<>();
            lines.subList(2, lines.size())
                    .forEach(line -> {
                        String key = line.substring(0, 3);
                        String left = line.substring(7, 10);
                        String right = line.substring(12, 15);
                        nodes.put(key, new Node(left, right));
                    });

            int result = 0;
            Node currentNode = nodes.get("AAA");
            Node endNode = nodes.get("ZZZ");
            while (currentNode != endNode) {
                char instruction = instructions.getNext();
                if (instruction == 'L') {
                    currentNode = nodes.get(currentNode.getLeft());
                } else {
                    currentNode = nodes.get(currentNode.getRight());
                }
                result++;
            }

            System.out.println("result: " + result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }
}