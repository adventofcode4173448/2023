package com.bernonconsulting;

public class Instructions {
    private char[] instructions = new char[]{};
    private int position = 0;

    public Instructions(char[] instructions) {
        this.instructions = instructions;
    }

    public char getNext() {
        if (position >= instructions.length){
            position = 0;
        }
        return instructions[position++];
    }
}
