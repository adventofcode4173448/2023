package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

//https://adventofcode.com/2023/day/8#part2
public class Main {

    private static Instructions instructions;
    private static Map<String, Node> nodes;

    public static void main(String[] args) {
        Instant start = Instant.now();
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            List<String> lines = bufferedReader.lines().toList();
            instructions = new Instructions(lines.get(0).toCharArray());
            nodes = new HashMap<>();

            lines.subList(2, lines.size())
                    .forEach(line -> {
                        String key = line.substring(0, 3);
                        String left = line.substring(7, 10);
                        String right = line.substring(12, 15);
                        nodes.put(key, new Node(key, left, right));
                    });

            List<Node> workingNodes = new ArrayList<>(nodes.values()
                    .stream()
                    .filter(Node::isStartNode)
                    .toList());

            long leastCommonMultiple = workingNodes.stream()
                    .map(Main::findStepsToEnd) //Steps to end is also steps to loop
                    .reduce(Main::findLeastCommonMultiple)
                    .orElseThrow();

            System.out.println("Result: " + leastCommonMultiple);
            System.out.println("Ran for " + Instant.now().minusMillis(start.toEpochMilli()).toEpochMilli() + "ms");
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static long findStepsToEnd(Node node) {
        instructions.reset();
        long result = 0;
        while (!node.isEndNode()) {
            node = switch (instructions.getNext()){
                case 'L' -> nodes.get(node.getLeft());
                case 'R' -> nodes.get(node.getRight());
                default -> throw new RuntimeException("Invalid instruction");
            };
            result++;
        }
        return result;
    }

    private static long findLeastCommonMultiple(long number1, long number2) {
        if (number1 == 0 || number2 == 0) {
            return 0;
        }
        long absHigherNumber = Math.max(number1, number2);
        long absLowerNumber = Math.min(number1, number2);
        long lcm = absHigherNumber;
        while (lcm % absLowerNumber != 0) {
            lcm += absHigherNumber;
        }
        return lcm;
    }
}