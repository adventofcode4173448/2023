package com.bernonconsulting;

public class Instructions {
    private char[] instructions;
    private int position = 0;

    public Instructions(char[] instructions) {
        this.instructions = instructions;
    }

    public char getNext() {
        if (position >= instructions.length) {
            reset();
        }
        return instructions[position++];
    }

    public void reset() {
        position = 0;
    }
}
