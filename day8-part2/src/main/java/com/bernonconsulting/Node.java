package com.bernonconsulting;

public class Node {

    private String identifier;
    private final String left;
    private final String right;
    private boolean isStartNode;
    private boolean isEndNode;

    public Node(String identifier, String left, String right) {
        this.identifier = identifier;
        this.left = left;
        this.right = right;
        this.isStartNode = identifier.endsWith("A");
        this.isEndNode = identifier.endsWith("Z");
    }

    public boolean isStartNode() {
        return isStartNode;
    }

    public boolean isEndNode() {
        return isEndNode;
    }

    public String getLeft() {
        return left;
    }

    public String getRight() {
        return right;
    }

    public String getIdentifier() {
        return this.identifier;
    }
}
