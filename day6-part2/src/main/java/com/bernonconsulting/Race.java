package com.bernonconsulting;

public class Race {
    private final long duration;
    private final long record;

    public Race(String duration, String record) {
        this.duration = Long.parseLong(duration);
        this.record = Long.parseLong(record);
    }

    public long getDuration() {
        return duration;
    }

    public long getRecord() {
        return record;
    }
}
