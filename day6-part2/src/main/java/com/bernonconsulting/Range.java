package com.bernonconsulting;

public class Range {
    private Long start;
    private Long end;

    public Range() {
    }

    public Range(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Long getDistance() {
        if (end - start > 0) {
            return end - start +1;
        }
        throw new RuntimeException("Negative distance");
    }
}
