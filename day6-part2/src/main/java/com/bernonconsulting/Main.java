package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/6#part2
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            List<String> lines = bufferedReader.lines().toList();
            String duration = lines.get(0).substring(lines.get(0).indexOf(":") +1).replaceAll(" ", "");
            String record = lines.get(1).substring(lines.get(1).indexOf(":") +1).replaceAll(" ", "");

            Race race = new Race(duration, record);
            Range range = new Range(findFirstWinningDuration(race), getLastWinningDuration(race));
            long distance = range.getDistance();

            System.out.println("result: " + distance);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static Long findFirstWinningDuration(Race race) {
        long maxDuration = race.getDuration();
        long minDistance = race.getRecord();
        for (long i = 0; i < maxDuration; i++) {
            if (i * (maxDuration - i) > minDistance) {
                return i;
            }
        }
        return -1l;
    }

    private static Long getLastWinningDuration(Race race) {
        long maxDuration = race.getDuration();
        long minDistance = race.getRecord();
        for (long i = maxDuration; i > 0; i--) {
            if (i * (maxDuration - i) > minDistance) {
                return i;
            }
        }
        return -1l;
    }
}