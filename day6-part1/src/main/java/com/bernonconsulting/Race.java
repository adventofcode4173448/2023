package com.bernonconsulting;

public class Race {
    private final int duration;
    private final int record;

    public Race(String duration, String record) {
        this.duration = Integer.parseInt(duration);
        this.record = Integer.parseInt(record);
    }

    public int getDuration() {
        return duration;
    }

    public int getRecord() {
        return record;
    }
}
