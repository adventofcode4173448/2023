package com.bernonconsulting;

public class Range {
    private Integer start;
    private Integer end;

    public Range() {
    }

    public Range(Integer start, Integer end) {
        this.start = start;
        this.end = end;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public Integer getDistance() {
        if (end - start > 0) {
            return end - start +1;
        }
        throw new RuntimeException("Negative distance");
    }
}
