package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/6
public class Main {

    private static Pattern numberPattern = Pattern.compile("\\d+");

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            List<String> lines = bufferedReader.lines().toList();
            Matcher durationMatcher = numberPattern.matcher(lines.get(0));
            Matcher recordMatcher = numberPattern.matcher(lines.get(1));

            List<Race> races = new ArrayList<>();

            while (durationMatcher.find() && recordMatcher.find()) {
                races.add(new Race(durationMatcher.group(), recordMatcher.group()));
            }

            List<Integer> resultList = races.stream()
                    .map(race -> new Range(findFirstWinningDuration(race), getLastWinningDuration(race)))
                    .map(Range::getDistance)
                    .toList();

            int result = resultList.stream().mapToInt(a -> a)
                    .reduce((a, b) -> a * b)
                    .orElseThrow();
            System.out.println("result: " + result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }


    private static Integer findFirstWinningDuration(Race race) {
        int maxDuration = race.getDuration();
        int minDistance = race.getRecord();
        for (int i = 0; i < maxDuration; i++) {
            if (i * (maxDuration - i) > minDistance) {
                return i;
            }
        }
        return -1;
    }

    private static Integer getLastWinningDuration(Race race) {
        int maxDuration = race.getDuration();
        int minDistance = race.getRecord();
        for (int i = maxDuration; i > 0; i--) {
            if (i * (maxDuration - i) > minDistance) {
                return i;
            }
        }
        return -1;
    }
}