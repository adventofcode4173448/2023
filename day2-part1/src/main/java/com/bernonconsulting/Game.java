package com.bernonconsulting;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private static final int MAX_RED_CUBES = 12;
    private static final int MAX_GREEN_CUBES = 13;
    private static final int MAX_BLUE_CUBES = 14;

    private final int id;
    private final List<String> pulls = new ArrayList<>();

    public Game(int id) {
        this.id = id;
    }

    public boolean isValidGame() {
        for (String pull : pulls) {
            for (String coloredPull : pull.trim().split(", ")) {
                int amount = Integer.parseInt(coloredPull.split(" ")[0]);
                Color color = Color.valueOf(coloredPull.split(" ")[1].toUpperCase());

                switch (color) {
                    case RED -> {
                        if (amount > MAX_RED_CUBES) {
                            return false;
                        }
                    }
                    case GREEN -> {
                        if (amount > MAX_GREEN_CUBES) {
                            return false;
                        }
                    }
                    case BLUE -> {
                        if (amount > MAX_BLUE_CUBES) {
                            return false;
                        }
                    }
                }
            }
        }
        System.out.println("game " + id + " is valid");
        return true;
    }


    public int getId() {
        return id;
    }

    public void addPull(String pull) {
        pulls.add(pull);
    }
}

enum Color {
    BLUE,
    RED,
    GREEN,
}