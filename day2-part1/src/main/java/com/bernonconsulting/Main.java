package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

//https://adventofcode.com/2023/day/2
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            List<Game> validGames = bufferedReader.lines()
                    .map(Main::fromOutput)
                    .filter(Game::isValidGame)
                    .toList();

            System.out.println(validGames.stream()
                    .mapToInt(Game::getId)
                    .sum());

        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static Game fromOutput(String output) {
        int id = Integer.parseInt(output.substring(output.indexOf(" ") + 1, output.indexOf(":")));
        Game game = new Game(id);
        String[] pulls = output.substring(output.indexOf(":") + 1).split(";");

        Arrays.stream(pulls)
                .forEach(game::addPull);

        return game;
    }
}
