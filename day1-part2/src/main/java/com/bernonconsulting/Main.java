package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/1#part2
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            int result = bufferedReader.lines()
                    .map(String::toLowerCase)
                    .map(line -> wordDigitToIntString(findFirstDigit(line)) + wordDigitToIntString(findLastDigit(line)))
                    .peek(System.out::println)
                    .mapToInt(Integer::valueOf)
                    .sum();
            System.out.println("The sum is: " + result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static String wordDigitToIntString(String input) {
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException exception) {
            return switch (input) {
                case "zero" -> "0";
                case "one" -> "1";
                case "two" -> "2";
                case "three" -> "3";
                case "four" -> "4";
                case "five" -> "5";
                case "six" -> "6";
                case "seven" -> "7";
                case "eight" -> "8";
                case "nine" -> "9";
                default -> throw new RuntimeException("Invalid number: " + input);
            };
        }
        return input;
    }

    private static String findFirstDigit(String line) {
        Matcher matcher = Pattern.compile("zero|one|two|three|four|five|six|seven|eight|nine|ten|\\d").matcher(line);
        matcher.find();
        return String.valueOf(matcher.group());
    }

    private static String findLastDigit(String line) {
        Matcher matcher = Pattern.compile("(zero|one|two|three|four|five|six|seven|eight|nine|ten|\\d)(?!(.*(zero|one|two|three|four|five|six|seven|eight|nine|ten|\\d))|((ne|wo|hree|ight|ine|en|\\d)))").matcher(line);
        matcher.find();
        return String.valueOf(matcher.group());
    }
}
