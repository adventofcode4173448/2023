package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/1
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            int result = bufferedReader.lines()
                    .map(line -> findFirstDigit(line) + findLastDigit(line))
                    .mapToInt(Integer::valueOf)
                    .sum();
            System.out.println("The sum is: " + result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static String findFirstDigit(String line) {
        Matcher matcher = Pattern.compile("\\d").matcher(line);
        matcher.find();
        return String.valueOf(matcher.group());
    }

    private static String findLastDigit(String line) {
        Matcher matcher = Pattern.compile("(\\d)(?!.*\\d)").matcher(line);
        matcher.find();
        return String.valueOf(matcher.group());
    }
}
