package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://adventofcode.com/2023/day/3
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {

            List<String> schematic = bufferedReader.lines().toList();
            int result = findPartNumbers(schematic)
                    .stream()
                    .mapToInt(a -> a)
                    .sum();
            System.out.println("result: " + result);
        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static List<Integer> findPartNumbers(List<String> schematic) {
        Pattern numberRegex = Pattern.compile("\\d+");
        List<Integer> partNumbers = new ArrayList<>();

        for (int rowNumber = 0; rowNumber < schematic.size(); rowNumber++) {
            String row = schematic.get(rowNumber);
            Matcher numberMatcher = numberRegex.matcher(row);
            while (numberMatcher.find()) {
                String currentNumber = numberMatcher.group();
                int startCol = numberMatcher.start();
                int endCol = numberMatcher.end();

                startCol = startCol > 0 ? startCol - 1 : startCol;
                endCol = endCol < 140 ? endCol + 1 : endCol;

                List<String> surroundingRows = schematic.subList(
                        Math.max(rowNumber - 1, 0),
                        Math.min(schematic.size(), rowNumber + 2)
                );

                if (isValidPartNumber(startCol, endCol, surroundingRows)) {
                    partNumbers.add(Integer.valueOf(currentNumber));
                }
            }
        }
        partNumbers.forEach(System.out::println);
        return partNumbers;
    }

    private static boolean isValidPartNumber(int startCol, int endCol, List<String> rows) {
        Pattern numberRegex = Pattern.compile("[^.\\d]");

        for (String row : rows) {
            String subString = row.substring(startCol, endCol);
            Matcher matcher = numberRegex.matcher(subString);
            if (matcher.find()) {
                return true;
            }
        }
        return false;
    }
}