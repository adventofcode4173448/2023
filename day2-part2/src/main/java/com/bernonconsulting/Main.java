package com.bernonconsulting;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

//https://adventofcode.com/2023/day/2#part2
public class Main {

    public static void main(String[] args) {
        ClassLoader classLoader = Main.class.getClassLoader();
        try (
                InputStream inputStream = classLoader.getResourceAsStream("input.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            int totalPower = bufferedReader.lines()
                    .map(Main::fromOutput)
                    .mapToInt(Game::getPower)
                    .sum();

            System.out.println(totalPower);

        } catch (IOException exception) {
            throw new RuntimeException("Error reading file.");
        }
    }

    private static Game fromOutput(String output) {
        Game game = new Game();
        String[] pulls = output.substring(output.indexOf(":") + 1).split(";");

        Arrays.stream(pulls).forEach(game::addPull);
        return game;
    }
}
