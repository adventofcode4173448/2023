package com.bernonconsulting;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private final List<String> pulls = new ArrayList<>();

    public void addPull(String pull) {
        pulls.add(pull);
    }

    public int getPower() {
        int usedReds = 0;
        int usedGreens = 0;
        int usedBlues = 0;

        for (String pull : pulls) {
            for (String coloredPull : pull.trim().split(", ")) {
                int amount = Integer.parseInt(coloredPull.split(" ")[0]);
                Color color = Color.valueOf(coloredPull.split(" ")[1].toUpperCase());

                switch (color) {
                    case RED -> {
                        if (amount > usedReds) {
                            usedReds = amount;
                        }
                    }
                    case GREEN -> {
                        if (amount > usedGreens) {
                            usedGreens = amount;
                        }
                    }
                    case BLUE -> {
                        if (amount > usedBlues) {
                            usedBlues = amount;
                        }
                    }
                }
            }
        }
        return usedReds * usedGreens * usedBlues;
    }
}

enum Color {
    BLUE,
    RED,
    GREEN,
}